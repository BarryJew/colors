export const getFiltersLS = () => {
  const json = window.localStorage.getItem("filters");
  return json ? JSON.parse(json) : [];
};

export const getSortLS = () => {
  const json = window.localStorage.getItem("sort");
  return json ? JSON.parse(json) : "price";
};

export const getCartLS = () => {
  const json = window.localStorage.getItem("cart");
  return json ? JSON.parse(json) : [];
};

export const setFiltersLS = (value) => {
  const json = JSON.stringify(value);
  window.localStorage.setItem("filters", json);
};
export const setSortLS = (value) => {
  const json = JSON.stringify(value);
  window.localStorage.setItem("sort", json);
};

export const setCartLS = (value) => {
  const json = JSON.stringify(value);
  window.localStorage.setItem("cart", json);
};
