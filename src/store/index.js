import { createStore } from "vuex";
import cart from "@/store/modules/cart";
import products from "@/store/modules/products";

export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    products,
    cart,
  },
});
