import mutations from "@/store/mutations";
import { getCartLS, setCartLS } from "@/utils/localStorageHandler";

const {
  CLEAR_CART,
  ADD_ITEM,
  PLUS_ITEM,
  MINUS_ITEM,
  REMOVE_TOGGLE,
  UPDATE_CART,
} = mutations;
const cartStore = {
  namespaced: true,
  state: {
    list: getCartLS(),
  },
  getters: {
    getCartList: ({ list }) => list,
    getTotalItems: ({ list }) => {
      return list.reduce((sum, obj) => {
        return !obj.inRemove ? obj.count + sum : sum;
      }, 0);
    },
    getTotalPrice: ({ list }) => {
      return list.reduce((sum, obj) => {
        return !obj.inRemove ? obj.count * obj.price + sum : sum;
      }, 0);
    },
  },
  mutations: {
    [CLEAR_CART](state) {
      state.list = [];
    },
    [PLUS_ITEM]({ list }, id) {
      list[id].count++;
    },
    [MINUS_ITEM]({ list }, id) {
      list[id].count--;
    },
    [ADD_ITEM]({ list }, item) {
      list.push(item);
    },
    [UPDATE_CART](state) {
      state.list = state.list.filter((item) => !item.inRemove);
    },
    [REMOVE_TOGGLE](state, id) {
      state.list[id].inRemove = !state.list[id].inRemove;
    },
  },
  actions: {
    addItem({ commit, getters }, data) {
      const cart = getters.getCartList;
      const findItemId = cart.findIndex((item) => item.id === data.id);
      if (findItemId >= 0) {
        commit(PLUS_ITEM, findItemId);
      } else {
        const newItem = {
          ...data,
          count: 1,
          inRemove: false,
        };
        commit(ADD_ITEM, newItem);
      }
      setCartLS(getters.getCartList);
    },
    minusItem({ commit, getters }, id) {
      const findItemId = getters.getCartList.findIndex(
        (item) => item.id === id
      );
      commit(MINUS_ITEM, findItemId);
      setCartLS(getters.getCartList);
    },
    plusItem({ getters, commit }, id) {
      const findItemId = getters.getCartList.findIndex(
        (item) => item.id === id
      );
      commit(PLUS_ITEM, findItemId);
      setCartLS(getters.getCartList);
    },
    removeToggle({ commit, getters }, id) {
      const findItemId = getters.getCartList.findIndex(
        (item) => item.id === id
      );
      commit(REMOVE_TOGGLE, findItemId);
    },
    updateCart({ commit, getters }) {
      commit(UPDATE_CART);
      setCartLS(getters.getCartList);
    },
    clearCart({ commit, getters }) {
      commit(CLEAR_CART);
      setCartLS(getters.getCartList);
    },
  },
};

export default cartStore;
