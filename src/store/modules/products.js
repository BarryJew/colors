import mutations from "@/store/mutations";
import {
  getSortLS,
  getFiltersLS,
  setSortLS,
  setFiltersLS,
} from "@/utils/localStorageHandler";

const { SET_PRODUCTS, SET_SORT, SET_FILTERS, ADD_FILTER, REMOVE_FILTER } =
  mutations;
const productsStore = {
  namespaced: true,
  state: {
    list: [],
    sort: getSortLS(),
    filters: getFiltersLS(),
  },
  getters: {
    getProductsList: ({ list }) => list,
    getProductsCount: ({ list }) => list.length,
    getFilters: ({ filters }) => filters,
    getSort: ({ sort }) => sort,
  },
  mutations: {
    [SET_PRODUCTS](state, data) {
      state.list = data;
    },
    [SET_PRODUCTS](state, data) {
      state.list = data;
    },
    [SET_SORT](state, data) {
      state.sort = data;
    },
    [SET_FILTERS](state, data) {
      state.filters = data;
    },
    [ADD_FILTER](state, data) {
      state.filters.push(data);
    },
    [REMOVE_FILTER](state, data) {
      state.filters = state.filters.filter((filter) => filter !== data);
    },
  },
  actions: {
    async fetchPizza({ commit, getters }) {
      const sortBy = getters.getSort.replace("-", "");
      const order = getters.getSort.includes("-") ? "asc" : "desc";
      const filters = getters.getFilters;
      let fetchedData = [];
      await fetch(
        `https://62e63434de23e2637928d58a.mockapi.io/products?sortBy=${sortBy}&order=${order}`
      )
        .then((response) => response.json())
        .then((data) => {
          fetchedData = data;
        });
      if (filters.length) {
        filters.forEach((type) => {
          fetchedData = fetchedData.filter((item) => item.type.includes(type));
        });
      }
      commit(SET_PRODUCTS, fetchedData);
      setSortLS(getters.getSort);
      setFiltersLS(getters.getFilters);
    },
    selectFilter({ commit, getters, dispatch }, data) {
      if (getters.getFilters.includes(data)) {
        commit(REMOVE_FILTER, data);
      } else {
        commit(ADD_FILTER, data);
      }
      dispatch("fetchPizza");
    },
    selectSort({ commit, dispatch }, data) {
      commit(SET_SORT, data);
      dispatch("fetchPizza");
    },
  },
};

export default productsStore;
